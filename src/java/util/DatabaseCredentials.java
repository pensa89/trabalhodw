package util;

public class DatabaseCredentials {
    
    private static final String DATABASE_URL = "jdbc:derby://localhost:1527/db";
    private static final String DATABASE_USER = "root";
    private static final String DATABASE_PASSWORD = "123";
    
    public static String getDatabaseURL() {
        return DATABASE_URL;
    }
    
    public static String getDatabaseUser() {
        return DATABASE_USER;
    }
    
    public static String getDatabasePassword() {
        return DATABASE_PASSWORD;
    }
}
