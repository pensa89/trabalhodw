package models;

import java.util.Date;

public class Reserva {
    
    private long id;
    private long hospedeiroId;
    private long hospedadoId;
    private int totalDeViajantes;
    private int totalDePraticantes;
    private Date dataInicio;
    private Date dataFim;
    private String status;
    
    public Reserva() { }
    
    public Reserva(long id, long hospedeiroId, long hospedadoId, Date dataInicio, Date dataFim, String status, int totalDeViajantes, int totalDePraticantes) {
        this.id = id;
        this.hospedeiroId = hospedeiroId;
        this.hospedadoId = hospedadoId;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.status = status;        
        this.totalDeViajantes = totalDeViajantes;
        this.totalDePraticantes = totalDePraticantes;
    }
    
    public Reserva(long hospedeiroId, long hospedadoId, Date dataInicio, Date dataFim, int totalDeViajantes, int totalDePraticantes) {
        this.hospedeiroId = hospedeiroId;
        this.hospedadoId = hospedadoId;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.totalDeViajantes = totalDeViajantes;
        this.totalDePraticantes = totalDePraticantes;
    }
    
    public long getId() {
        return this.id;
    }
    
    public long getHospedeiroId() {
        return this.hospedeiroId;
    }
    
    public long getHospedadoId() {
        return this.hospedadoId;
    }
    
    public java.sql.Date getDataInicio() {
        return new java.sql.Date(this.dataInicio.getTime());
    }
    
    public java.sql.Date getDataFim() {
        return new java.sql.Date(this.dataFim.getTime());
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public int getTotalDeViajantes() {
        return this.totalDeViajantes;
    }
    
    public int getTotalDePraticantes() {
        return this.totalDePraticantes;
    }
}
