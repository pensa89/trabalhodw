package models;

public class Avaliacao {
    
    private long id;
    private int nota;
    private long avaliadorId;
    private long avaliadoId;
    private TipoAvaliacao tipoAvaliacao;
    
    public Avaliacao(long avaliado, long avaliador, int nota, TipoAvaliacao tipoAvaliacao) {
        this.avaliadorId = avaliador;
        this.avaliadoId = avaliado;
        this.nota = nota;
        this.tipoAvaliacao = tipoAvaliacao;
    }
    
    public Avaliacao(long id, long avaliador, long avaliado, int nota, TipoAvaliacao tipoAvaliacao) {
        this.id = id;
        this.avaliadorId = avaliador;
        this.avaliadoId = avaliado;
        this.nota = nota;
        this.tipoAvaliacao = tipoAvaliacao;
    }
     
    public long getId() {
        return this.id;
    }
    
    public int getNota() {
        return this.nota;
    }
    
    public long getAvaliadorId() {
        return this.avaliadorId;
    }
    
    public long getAvaliadoId() {
        return this.avaliadoId;
    }
    
    public TipoAvaliacao getTipoAvaliacao() {
        return this.tipoAvaliacao;
    }
}
