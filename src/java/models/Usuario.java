package models;

import java.io.Serializable;

public class Usuario implements Serializable {

    private Long id;
    private String nome;
    private String username;
    private String senha;
    private String endereco;
    
    public Usuario(long id, String nome, String endereco, String username, String senha) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
        this.username = username;
        this.senha = senha;
    }
    
    public Usuario(long id, String nome, String endereco) {
        this.id = id;
        this.nome = nome;
        this.endereco = endereco;
    }
    
    public Usuario(long id, String nome, String endereco, String username) {
        this.id = id;
        this.nome = nome;
        this.username = username;
        this.endereco = endereco;
    }
    
    public Usuario(String nome, String endereco, String username, String senha) {
        this.nome = nome;
        this.endereco = endereco;
        this.username = username;
        this.senha = senha;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }
    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getSenha() {
        return this.senha;
    }
}
