package models;

public class EsporteFavorito {
    
    private long usuarioId;
    private long esporteId;
    
    public EsporteFavorito(long usuarioId, long esporteId) {
        this.usuarioId = usuarioId;
        this.esporteId = esporteId;
    }
    
    public long getUsuarioId() {
        return this.usuarioId;
    }
    
    public long getEsporteId() {
        return this.esporteId;
    }
}
