package models;

public class Esporte {
    
    private long id;
    private String nome;
    
    public Esporte(long id, String nome) {
        this.id=  id;
        this.nome = nome;
    }
    
    public Esporte(String nome) {
        this.nome = nome;
    }
    
    public long getId() {
        return this.id;
    }
    
    public String getNome() {
        return this.nome;
    }
}
