package models;

import java.util.ArrayList;
import java.util.List;

public enum TipoAvaliacao {
    AMIGO(0),
    HOSPEDADO(1),
    HOSPEDEIRO(2),
    CARONEIRO(3),
    CARONA(4);
    
    private final int tipoAvaliacao;
    
    TipoAvaliacao(int tipoAvaliacao) {
        this.tipoAvaliacao = tipoAvaliacao;
    }
    
    public int getValorTipoAvaliacao() {
        return this.tipoAvaliacao;
    }
    
    public static TipoAvaliacao getTipoAvaliacaoEnum(int tipo) {
        TipoAvaliacao tipoAvaliacao = null;
        
        switch(tipo) {
            case 0:
                tipoAvaliacao = TipoAvaliacao.AMIGO;
                break;
            case 1:
                tipoAvaliacao = TipoAvaliacao.HOSPEDADO;
                break;
            case 2:
                tipoAvaliacao = TipoAvaliacao.HOSPEDEIRO;
                break;
            case 3:
                tipoAvaliacao = TipoAvaliacao.CARONEIRO;
                break;    
            case 4:
                tipoAvaliacao = TipoAvaliacao.CARONA;
                break;
            default:
                return null;
        }
        
        return tipoAvaliacao;
    }
    
    public static TipoAvaliacao getTipoAvaliacaoComplementar(int tipo) {
        TipoAvaliacao tipoAvaliacao = null;
        
        switch(tipo) {
            case 1:
                tipoAvaliacao = TipoAvaliacao.HOSPEDEIRO;
                break;
            case 2:
                tipoAvaliacao = TipoAvaliacao.HOSPEDADO;
                break;
            case 3:
                tipoAvaliacao = TipoAvaliacao.CARONA;
                break;    
            case 4:
                tipoAvaliacao = TipoAvaliacao.CARONEIRO;
                break;
            default:
                return null;
        }
        
        return tipoAvaliacao;
    }
    
    @Override
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase();
    }
    
    public static List<TipoAvaliacao> getTodosTiposAvaliacao() {
        List<TipoAvaliacao> tiposAvaliacao = new ArrayList<>();
        tiposAvaliacao.add(TipoAvaliacao.AMIGO);
        tiposAvaliacao.add(TipoAvaliacao.HOSPEDADO);
        tiposAvaliacao.add(TipoAvaliacao.HOSPEDEIRO);
        tiposAvaliacao.add(TipoAvaliacao.CARONEIRO);
        tiposAvaliacao.add(TipoAvaliacao.CARONA);
                
        return tiposAvaliacao;
    }
}
