package models;

import java.util.Objects;

public class Hospedeiro extends Usuario {
    
    private long id;
    private int lotacao;
    private boolean estaRecebendoViajantes;
    private String pais;
    private String cidade;
    
    public Hospedeiro(long id, long usuarioId, String nome, String endereco, String username, boolean estaRecebendoViajantes, int lotacao, String pais, String cidade) {
        super(usuarioId, nome, endereco, username);
        this.id = id;
        this.lotacao = lotacao;
        this.estaRecebendoViajantes = estaRecebendoViajantes;
        this.cidade = cidade;
        this.pais = pais;
    }
    
    public Hospedeiro(long usuarioId, String nome, String endereco, String username, String senha, boolean estaRecebendoViajantes, int lotacao, String pais, String cidade) {
        super(usuarioId, nome, endereco, username, senha);
        this.lotacao = lotacao;
        this.estaRecebendoViajantes = estaRecebendoViajantes;
        this.cidade = cidade;
        this.pais = pais;
    }
    
    public Hospedeiro(String nome, String endereco, String username, String senha, boolean estaRecebendoViajantes, int lotacao, String pais, String cidade) {
        super(nome, endereco, username, senha);
        this.lotacao = lotacao;
        this.estaRecebendoViajantes = estaRecebendoViajantes;
        this.cidade = cidade;
        this.pais = pais;
    }
    
    public long getHospedeiroId() {
        return this.id;
    }
    
    public long getUsuarioId() {
        return super.getId();
    }
 
    public int getLotacao() {
        return this.lotacao;
    }
    
    public boolean getEstaRecebendoViajantes() {
        return this.estaRecebendoViajantes;
    }
    
    public String getPais() {
        return this.pais;
    }
    
    public String getCidade() {
        return this.cidade;
    }
    
    @Override
    public boolean equals(Object o) {
        Hospedeiro h = (Hospedeiro) o;
        if(h == null) {
            return false;
        }
        
        return this.id == h.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 61 * hash + this.lotacao;
        hash = 61 * hash + Objects.hashCode(this.pais);
        hash = 61 * hash + Objects.hashCode(this.cidade);
        return hash;
    }
}
