package models;

import java.io.Serializable;
import java.util.Objects;

public class Amigos implements Serializable {
    
    private Long id;
    private Long usuarioId1;
    private Long usuarioId2;
    
    public Amigos(long id, long usuarioId1, long usuarioId2) {
        this.id = id;
        this.usuarioId1 = usuarioId1;
        this.usuarioId2 = usuarioId2;
    }
    
    public Amigos(long usuarioId1, long usuarioId2) {
        this.usuarioId1 = usuarioId1;
        this.usuarioId2 = usuarioId2;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUsuarioId1() {
        return usuarioId1;
    }

    public void setUsuarioId1(Long usuarioId1) {
        this.usuarioId1 = usuarioId1;
    }

    public Long getUsuarioId2() {
        return usuarioId2;
    }

    public void setUsuarioId2(Long usuarioId2) {
        this.usuarioId2 = usuarioId2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Amigos)) {
            return false;
        }
        Amigos other = (Amigos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        
        if(!Objects.equals(this.usuarioId1, other.usuarioId1) && !Objects.equals(this.usuarioId2, other.usuarioId2)) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "models.Amigos[ id=" + id + " ]";
    }    
}
