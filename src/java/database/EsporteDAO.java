package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Esporte;

public class EsporteDAO {
    
    private static EsporteDAO instancia = null;
    
    private EsporteDAO() { }
    
    public static EsporteDAO getInstancia() {
        if(instancia == null) {
            instancia = new EsporteDAO();
        }
        return instancia;
    }
    
    public List<Esporte> getEsportes(Connection conn) {   
        List<Esporte> esportes = new ArrayList<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM esportes")) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");;
                String nome = resultado.getString("nome");
                
                Esporte e = new Esporte(id, nome);                
                esportes.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EsporteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return esportes;
        }        
    }  
    
    public Esporte getEsporte(Connection conn, long esporteId) {   
        Esporte esporte = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM esportes WHERE id = " + esporteId)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");;
                String nome = resultado.getString("nome");
                
                esporte = new Esporte(id, nome);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EsporteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return esporte;
        }        
    }    
}
