package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Hospedeiro;

public class HospedeiroDAO {
    
    private static HospedeiroDAO instancia = null;
    
    private HospedeiroDAO() { }
    
    public static HospedeiroDAO getInstancia() {
        if(instancia == null) {
            instancia = new HospedeiroDAO();
        }
        return instancia;
    }
    
    public Hospedeiro getHospedeiroPorUsuarioId(Connection conn, long usuarioIdQuery) {   
        Hospedeiro hospedeiro = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM hospedeiros h, usuarios u WHERE u.id = h.usuario_id AND h.usuario_id = " + usuarioIdQuery)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long usuarioId = resultado.getLong("usuario_id");
                String nome = resultado.getString("nome");
                String username = resultado.getString("username");
                String endereco = resultado.getString("endereco");
                int lotacao = resultado.getInt("lotacao");
                boolean estaRecebendoViajantes = resultado.getBoolean("recebendo_viajantes");
                String pais = resultado.getString("pais");
                String cidade = resultado.getString("cidade");
                
                hospedeiro = new Hospedeiro(id, usuarioId, nome, endereco, username, estaRecebendoViajantes, lotacao, pais, cidade);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HospedeiroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return hospedeiro;
        }        
    } 
    
    public Hospedeiro getHospedeiroPorId(Connection conn, long hospedeiroIdQuery) {   
        Hospedeiro hospedeiro = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM hospedeiros h, usuarios u WHERE u.id = h.usuario_id AND h.id = " + hospedeiroIdQuery)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                long usuarioId = resultado.getLong("usuario_id");
                String nome = resultado.getString("nome");
                String username = resultado.getString("username");
                String endereco = resultado.getString("endereco");
                int lotacao = resultado.getInt("lotacao");
                boolean estaRecebendoViajantes = resultado.getBoolean("recebendo_viajantes");
                String pais = resultado.getString("pais");
                String cidade = resultado.getString("cidade");
                
                hospedeiro = new Hospedeiro(id, usuarioId, nome, endereco, username, estaRecebendoViajantes, lotacao, pais, cidade);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HospedeiroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return hospedeiro;
        }        
    }
    
    public Set<Hospedeiro> getHospedeirosPorQuery(Connection conn, String paisQuery, String cidadeQuery) {
        Set<Hospedeiro> hospedeiros = new HashSet<>();
        
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM hospedeiros h, usuarios u WHERE u.id = h.usuario_id AND h.recebendo_viajantes = true AND h.pais = '" + paisQuery + "' AND h.cidade = '" + cidadeQuery + "'")) {
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                long usuarioId = resultado.getLong("usuario_id");
                String nome = resultado.getString("nome");
                String username = resultado.getString("username");
                String endereco = resultado.getString("endereco");
                int lotacao = resultado.getInt("lotacao");
                boolean estaRecebendoViajantes = resultado.getBoolean("recebendo_viajantes");
                String pais = resultado.getString("pais");
                String cidade = resultado.getString("cidade");
                
                Hospedeiro h = new Hospedeiro(id, usuarioId, nome, endereco, username, estaRecebendoViajantes, lotacao, pais, cidade);
                hospedeiros.add(h);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HospedeiroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return hospedeiros;
        }
    }
    
    public Hospedeiro insereNovoHospedeiro(Connection conn, Hospedeiro h) {
        Hospedeiro novoHospedeiro = null;        
        try {
            PreparedStatement sql = conn.prepareStatement("INSERT INTO hospedeiros (USUARIO_ID, LOTACAO, RECEBENDO_VIAJANTES) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);            
                        
            sql.setLong(1, h.getUsuarioId());
            sql.setInt(2, h.getLotacao());
            sql.setBoolean(3, h.getEstaRecebendoViajantes());
        
            sql.setMaxRows(1);
            sql.executeUpdate();
            
            ResultSet resultado = sql.getGeneratedKeys();
            if(resultado.next()) {
                long id = resultado.getLong(1);
                novoHospedeiro = this.getHospedeiroPorId(conn, sql.getGeneratedKeys().getLong("id"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(HospedeiroDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return novoHospedeiro;
        }        
    }
}
