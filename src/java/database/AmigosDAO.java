package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Amigos;

public class AmigosDAO {
    
    private static AmigosDAO instancia = null;
    
    private AmigosDAO() { }
    
    public static AmigosDAO getInstancia() {
        if(instancia == null) {
            instancia = new AmigosDAO();
        }
        return instancia;
    }
    
    public Set<Amigos> getAmigosDe(Connection conn, long usuarioId) {   
        Set<Amigos> amigos = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM amigos WHERE usuario_id1 = " + usuarioId + " OR usuario_id2 = " + usuarioId)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");;
                long usuarioId1 = resultado.getLong("usuario_id1");;
                long usuarioId2 = resultado.getLong("usuario_id2");;
                
                amigos.add(new Amigos(id, usuarioId1, usuarioId2));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AmigosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return amigos;
        }        
    }    
}
