package database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Reserva;

public class ReservasDAO {
    
    private static ReservasDAO instancia = null;
    
    private ReservasDAO() { }
    
    public static ReservasDAO getInstancia() {
        if(instancia == null) {
            instancia = new ReservasDAO();
        }
        return instancia;
    }
    
    public Set<Reserva> getReservasDeHospedeiro(Connection conn, long hospedeiroIdQuery) {   
        Set<Reserva> reservas = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM reservas WHERE status = 'pendente' AND hospedeiro_id = " + hospedeiroIdQuery)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long hospedadoId = resultado.getLong("hospedado_id");
                long hospedeiroId = resultado.getLong("hospedeiro_id");
                int totalDeViajantes = resultado.getInt("total_viajantes");
                int totalDePraticantes = resultado.getInt("total_praticantes");
                Date dataInicio = resultado.getDate("data_inicio");
                Date dataFim  = resultado.getDate("data_fim");
                String status = resultado.getString("status");
                
                Reserva r = new Reserva(id, hospedeiroId, hospedadoId, dataInicio, dataFim, status, totalDeViajantes, totalDePraticantes);
                reservas.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return reservas;
        }        
    } 
    
    public Set<Reserva> getReservasDeUsuario(Connection conn, long usuarioIdQuery) {   
        Set<Reserva> reservas = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM reservas WHERE hospedado_id = " + usuarioIdQuery)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long hospedadoId = resultado.getLong("hospedado_id");
                long hospedeiroId = resultado.getLong("hospedeiro_id");
                int totalDeViajantes = resultado.getInt("total_viajantes");
                int totalDePraticantes = resultado.getInt("total_praticantes");
                Date dataInicio = resultado.getDate("data_inicio");
                Date dataFim  = resultado.getDate("data_fim");
                String status = resultado.getString("status");
                
                Reserva r = new Reserva(id, hospedeiroId, hospedadoId, dataInicio, dataFim, status, totalDeViajantes, totalDePraticantes);
                reservas.add(r);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return reservas;
        }        
    }
    
    public Reserva getReservaPorId(Connection conn, long reservaId) {   
        Reserva reserva = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM reservas WHERE id = " + reservaId)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long hospedadoId = resultado.getLong("hospedado_id");
                long hospedeiroId = resultado.getLong("hospedeiro_id");
                int totalDeViajantes = resultado.getInt("total_viajantes");
                int totalDePraticantes = resultado.getInt("total_praticantes");
                Date dataInicio = resultado.getDate("data_inicio");
                Date dataFim  = resultado.getDate("data_fim");
                String status = resultado.getString("status");
                
                reserva = new Reserva(id, hospedeiroId, hospedadoId, dataInicio, dataFim, status, totalDeViajantes, totalDePraticantes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return reserva;
        }        
    } 
    
    public Reserva getReservaNoPeriodo(Connection conn, long hospedeiroIdQuery, java.util.Date dataInicioQuery, java.util.Date dataFimQuery) {
        java.sql.Date dataInicioSQL = new java.sql.Date(dataInicioQuery.getTime());
        java.sql.Date dataFimSQL= new java.sql.Date(dataFimQuery.getTime());
        
        Reserva reserva = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM reservas WHERE hospedeiro_id = " + hospedeiroIdQuery + " AND status == 'aprovada' AND ((data_inicio <= " + dataInicioSQL + " AND data_fim >= " + dataFimSQL + ") OR (data_inicio >= " + dataInicioSQL + " AND data_fim <= " + dataFimSQL + "))")) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long hospedadoId = resultado.getLong("hospedado_id");
                long hospedeiroId = resultado.getLong("hospedeiro_id");
                int totalDeViajantes = resultado.getInt("total_viajantes");
                int totalDePraticantes = resultado.getInt("total_praticantes");
                Date dataInicio = resultado.getDate("data_inicio");
                Date dataFim  = resultado.getDate("data_fim");
                String status = resultado.getString("status");
                
                reserva = new Reserva(id, hospedeiroId, hospedadoId, dataInicio, dataFim, status, totalDeViajantes, totalDePraticantes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return reserva;
        }        
    }
    
    public Reserva insereNovaReserva(Connection conn, Reserva r) {
        Reserva novaReserva = null;        
        try {
            PreparedStatement sql = conn.prepareStatement("INSERT INTO reservas (HOSPEDADO_ID, HOSPEDEIRO_ID, DATA_INICIO, DATA_FIM, TOTAL_VIAJANTES, TOTAL_PRATICANTES) VALUES (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);            
                        
            sql.setLong(1, r.getHospedadoId());
            sql.setLong(2, r.getHospedeiroId());
            sql.setDate(3, r.getDataInicio());
            sql.setDate(4, r.getDataFim());
            sql.setInt(5, r.getTotalDeViajantes());
            sql.setInt(6, r.getTotalDePraticantes());
        
            sql.setMaxRows(1);
            sql.executeUpdate();
            
            ResultSet resultado = sql.getGeneratedKeys();
            if(resultado.next()) {
                long id = resultado.getLong(1);
                novaReserva = this.getReservaPorId(conn, sql.getGeneratedKeys().getLong("id"));
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return novaReserva;
        }        
    }  
    
    public void atualizaStatusReserva(Connection conn, long reservaId, String status) {
        try {
            PreparedStatement sql = conn.prepareStatement("UPDATE reservas SET status = '" + status + "' WHERE id = " + reservaId);
            sql.executeUpdate();       
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void cancelarReservasPendentes(Connection conn, long usuarioId) {
        try {
            PreparedStatement sql = conn.prepareStatement("UPDATE reservas SET status = 'cancelada' WHERE id = " + usuarioId + " AND status = 'pendente'");
            sql.executeUpdate();       
        } catch (SQLException ex) {
            Logger.getLogger(ReservasDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }        
}
