package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Esporte;
import models.EsporteFavorito;

public class EsportesFavoritosDAO {
    
    private static EsportesFavoritosDAO instancia = null;
    
    private EsportesFavoritosDAO() { }
    
    public static EsportesFavoritosDAO getInstancia() {
        if(instancia == null) {
            instancia = new EsportesFavoritosDAO();
        }
        return instancia;
    }
    
    public EsporteFavorito getEsportesFavoritoPorId(Connection conn, long uId) {   
        EsporteFavorito esporteFavorito = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM esportes_favoritos WHERE id = " + uId)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long usuarioId = resultado.getLong("usuario_id");
                long esporteId = resultado.getLong("esporte_id");
                
                esporteFavorito = new EsporteFavorito(usuarioId, esporteId);
            }
        } catch (SQLException ex) {
            Logger.getLogger(EsportesFavoritosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return esporteFavorito;
        }        
    }
    
    public Set<EsporteFavorito> getEsportesFavoritosDe(Connection conn, long uId) {   
        Set<EsporteFavorito> esportesFavoritos = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM esportes_favoritos WHERE usuario_id = " + uId)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long usuarioId = resultado.getLong("usuario_id");
                long esporteId = resultado.getLong("esporte_id");
                
                esportesFavoritos.add(new EsporteFavorito(usuarioId, esporteId));
            }
        } catch (SQLException ex) {
            Logger.getLogger(EsportesFavoritosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return esportesFavoritos;
        }        
    }
    
    public Set<Esporte> getEsportesDe(Connection conn, Set<EsporteFavorito> esportesFavoritos) {   
        Set<Esporte> esportes = new HashSet<>();
        
        for(EsporteFavorito ef : esportesFavoritos) {
            try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM esportes WHERE id = " + ef.getEsporteId())) {
                ResultSet resultado = sql.executeQuery();            
                while(resultado.next()) {
                    long id = resultado.getLong("id");
                    String nome = resultado.getString("nome");

                    esportes.add(new Esporte(id, nome));
                }
            } catch (SQLException ex) {
                Logger.getLogger(EsportesFavoritosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return esportes;        
    }
    
    public EsporteFavorito insereNovoEsporteFavorito(Connection conn, long uId, long eId) {
        EsporteFavorito esporteFavorito = null;        
        try {
            PreparedStatement sql = conn.prepareStatement("INSERT INTO ESPORTES_FAVORITOS (USUARIO_ID, ESPORTE_ID) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);            
                        
            sql.setLong(1, uId);
            sql.setLong(2, eId);
            
            sql.setMaxRows(1);
            sql.executeUpdate();            
            
            ResultSet resultado = sql.getGeneratedKeys();
            if(resultado.next()) {
                long id = resultado.getLong(1);
                esporteFavorito = this.getEsportesFavoritoPorId(conn, sql.getGeneratedKeys().getLong("id"));
            } 
        } catch (SQLException ex) {
            Logger.getLogger(EsportesFavoritosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return esporteFavorito;
        }        
    }
}
