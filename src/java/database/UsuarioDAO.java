package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Usuario;

public class UsuarioDAO {
    
    private static UsuarioDAO instancia = null;
    
    private UsuarioDAO() { }
    
    public static UsuarioDAO getInstancia() {
        if(instancia == null) {
            instancia = new UsuarioDAO();
        }
        return instancia;
    }
    
    public Usuario getUsuarioPorId(Connection conn, long usuarioId) {   
        Usuario usuario = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM usuarios WHERE id = " + usuarioId)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                String nome = resultado.getString("nome");
                String username = resultado.getString("username");
                String endereco = resultado.getString("endereco");
                usuario = new Usuario(id, nome, endereco, username);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return usuario;
        }        
    }
    
    public Usuario getUsuarioNoLogin(Connection conn, String username, String senha) {   
        Usuario usuario = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM usuarios WHERE username = '" + username + "' AND senha = '" + senha + "'")) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                String nome = resultado.getString("nome");
                String endereco = resultado.getString("endereco");
                usuario = new Usuario(id, nome, endereco);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return usuario;
        }        
    }
    
    public Usuario getUsuarioPorUsername(Connection conn, String username) {   
        Usuario usuario = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM usuarios WHERE username = '" + username + "'")) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                String nome = resultado.getString("nome");
                String endereco = resultado.getString("endereco");
                usuario = new Usuario(id, nome, endereco);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return usuario;
        }        
    }
    
    public Set<Usuario> getTodosOsUsuariosExceto(Connection conn, long usuarioId) {
        Set<Usuario> usuarios = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM usuarios WHERE id != " + usuarioId)) {            
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                String nome = resultado.getString("nome");
                String endereco = resultado.getString("endereco");
                Usuario u = new Usuario(id, nome, endereco);
                
                usuarios.add(u);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return usuarios;
        }  
    }
    
    public Usuario insereNovoUsuario(Connection conn, Usuario u) {
        Usuario novoUsuario = null;        
        try {
            PreparedStatement sql = conn.prepareStatement("INSERT INTO usuarios (NOME, USERNAME, SENHA, ENDERECO) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);            
                        
            sql.setString(1, u.getNome());
            sql.setString(2, u.getUsername());
            sql.setString(3, u.getSenha());
            sql.setString(4, u.getEndereco());
        
            sql.setMaxRows(1);
            sql.executeUpdate();
            
            ResultSet resultado = sql.getGeneratedKeys();
            if(resultado.next()) {
                long id = resultado.getLong(1);
                novoUsuario = this.getUsuarioPorId(conn, id);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return novoUsuario;
        }        
    }
}
