package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Avaliacao;
import models.TipoAvaliacao;

public class AvaliacaoDAO {
    
    private static AvaliacaoDAO instancia = null;
    
    private AvaliacaoDAO() { }
    
    public static AvaliacaoDAO getInstancia() {
        if(instancia == null) {
            instancia = new AvaliacaoDAO();
        }
        return instancia;
    }
    
    public Avaliacao getAvaliacaoPorId(Connection conn, long avaliacaoId) {   
        Avaliacao avaliacao = null;
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM avaliacoes WHERE id = " + avaliacaoId)) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                long avaliado = resultado.getLong("avaliado_id");
                long avaliador = resultado.getLong("avaliador_id");
                int nota = resultado.getInt("nota");
                int tipoAvaliacao = resultado.getInt("tipo");                
                
                avaliacao = new Avaliacao(id, avaliador, avaliado, nota, TipoAvaliacao.getTipoAvaliacaoEnum(tipoAvaliacao));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return avaliacao;
        }        
    }

    public Set<Avaliacao> getAvaliacoesSobre(Connection conn, long avaliadoId) {   
        Set<Avaliacao> avaliacoes = new HashSet<>();
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM avaliacoes WHERE avaliado_id = " + avaliadoId)) {
            ResultSet resultado = sql.executeQuery();            
            while(resultado.next()) {
                long id = resultado.getLong("id");
                long avaliado = resultado.getLong("avaliado_id");
                long avaliador = resultado.getLong("avaliador_id");
                int nota = resultado.getInt("nota");
                int tipoAvaliacao = resultado.getInt("tipo");                
                
                avaliacoes.add(new Avaliacao(id, avaliador, avaliado, nota, TipoAvaliacao.getTipoAvaliacaoEnum(tipoAvaliacao)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return avaliacoes;
        }        
    }
    
    public Avaliacao getAvaliacaoComplementar(Connection conn, long avaliadoId, long avaliadorId, int tipoAvaliacaoValor) {
        Avaliacao avaliacaoComplementar = null;
        TipoAvaliacao tipoAvaliacaoComplementar = TipoAvaliacao.getTipoAvaliacaoComplementar(tipoAvaliacaoValor);
        
        if(tipoAvaliacaoComplementar == null) {
            return null;
        }
        
        try (PreparedStatement sql = conn.prepareStatement("SELECT * FROM avaliacoes WHERE avaliado_id = " + avaliadorId + " AND avaliador_id = " + avaliadoId + " AND tipo = " + tipoAvaliacaoComplementar.getValorTipoAvaliacao())) {
            sql.setMaxRows(1);
            ResultSet resultado = sql.executeQuery();            
            if(resultado.next()) {
                long id = resultado.getLong("id");
                long avaliado = resultado.getLong("avaliado_id");
                long avaliador = resultado.getLong("avaliador_id");
                int nota = resultado.getInt("nota");
                int tipoAvaliacao = resultado.getInt("tipo");                
                
                avaliacaoComplementar = new Avaliacao(id, avaliador, avaliado, nota, TipoAvaliacao.getTipoAvaliacaoEnum(tipoAvaliacao));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return avaliacaoComplementar;
        }
    }
    
    public Avaliacao insereNovaAvaliacao(Connection conn, Avaliacao a) {
        Avaliacao novaAvaliacao = null;
        try {
            PreparedStatement sql = conn.prepareStatement("INSERT INTO avaliacoes (AVALIADO_ID, AVALIADOR_ID, NOTA, TIPO) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            
            sql.setLong(1, a.getAvaliadoId());
            sql.setLong(2, a.getAvaliadorId());
            sql.setInt(3, a.getNota());
            sql.setInt(4, a.getTipoAvaliacao().getValorTipoAvaliacao());
        
            sql.executeUpdate();
                        
            ResultSet resultado = sql.getGeneratedKeys();
            if(resultado.next()) {
                long id = resultado.getLong(1);
                novaAvaliacao = this.getAvaliacaoPorId(conn, sql.getGeneratedKeys().getLong("id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AvaliacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return novaAvaliacao;
        }
    }
}
