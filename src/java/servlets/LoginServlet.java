package servlets;

import database.AmigosDAO;
import database.EsportesFavoritosDAO;
import database.HospedeiroDAO;
import database.UsuarioDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Amigos;
import models.Esporte;
import models.EsporteFavorito;
import models.Hospedeiro;
import models.Usuario;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;


@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String username = request.getParameter("username");
        String senha = request.getParameter("senha");
        
        Usuario usuarioAtual = UsuarioDAO.getInstancia().getUsuarioNoLogin(conn, username, senha);
        HttpSession session = request.getSession();
        
        if(usuarioAtual == null) {
            JSONObject errosJSON = new JSONObject();
            errosJSON.put("mensagem", "Credenciais inválidas, tente novamente.");

            session.setAttribute("erros", errosJSON);
            
            response.sendRedirect("");
        } else {            
            long usuarioId = usuarioAtual.getId();

            session.removeAttribute("erros");
            session.setAttribute("usuarioLogadoId", usuarioId);

            response.sendRedirect("dashboard");
        }        
    }    
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }       
}
