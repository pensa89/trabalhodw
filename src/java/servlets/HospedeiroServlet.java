package servlets;

import database.HospedeiroDAO;
import database.ReservasDAO;
import database.UsuarioDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Hospedeiro;
import models.Reserva;
import models.Usuario;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;

@WebServlet(name = "HospedeiroServlet", urlPatterns = {"/hospedeiro"})
public class HospedeiroServlet extends HttpServlet {
    
    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        long usuarioId = (long) session.getAttribute("usuarioLogadoId");
        
        Hospedeiro hospedeiro = HospedeiroDAO.getInstancia().getHospedeiroPorUsuarioId(conn, usuarioId);        
        Set<Reserva> reservasDoHospedeiro = ReservasDAO.getInstancia().getReservasDeHospedeiro(conn, hospedeiro.getHospedeiroId());
                       
        JSONObject reservasInfoJSON = new JSONObject();
        JSONArray reservasJSONArray = this.buildReservasJSONArray(reservasDoHospedeiro);        
        reservasInfoJSON.put("reservas", reservasJSONArray);

        session.setAttribute("reservasInfoJSON", reservasInfoJSON);
          
        response.sendRedirect("hospedeiro.jsp");
    }
    
    private JSONArray buildReservasJSONArray(Set<Reserva> reservas) {
        JSONArray reservasJSONArray = new JSONArray();
        
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        
        for(Reserva r : reservas) {
            JSONObject reservaAtual = new JSONObject();
            Usuario u = UsuarioDAO.getInstancia().getUsuarioPorId(conn, r.getHospedadoId());
            reservaAtual.put("idUsuario", u.getId());
            reservaAtual.put("nome", u.getNome());
            reservaAtual.put("idReserva", r.getId());
            reservaAtual.put("dataInicio", df.format(r.getDataInicio()));
            reservaAtual.put("dataFim", df.format(r.getDataFim()));
            reservaAtual.put("totalDePraticantes", r.getTotalDePraticantes());
            reservaAtual.put("totalDeViajantes", r.getTotalDeViajantes());
            
            reservasJSONArray.add(reservaAtual);
        }        
        return reservasJSONArray;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }
}
