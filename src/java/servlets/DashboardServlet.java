package servlets;

import database.AmigosDAO;
import database.AvaliacaoDAO;
import database.EsportesFavoritosDAO;
import database.HospedeiroDAO;
import database.UsuarioDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Amigos;
import models.Avaliacao;
import models.Esporte;
import models.EsporteFavorito;
import models.Hospedeiro;
import models.TipoAvaliacao;
import models.Usuario;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;

@WebServlet(name = "DashboardServlet", urlPatterns = {"/dashboard"})
public class DashboardServlet extends HttpServlet {
    
    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               
        HttpSession session = request.getSession();
        
        long usuarioId = (long) session.getAttribute("usuarioLogadoId");
        boolean isUsuarioLogado = true;       
        
        Object queryStringId = request.getParameter("id");
        
        if(queryStringId != null && Long.parseLong((String) queryStringId) != usuarioId) {
            isUsuarioLogado = false;
            usuarioId = Long.parseLong(request.getParameter("id"));
        }      
        
        boolean isHospedeiro = HospedeiroDAO.getInstancia().getHospedeiroPorUsuarioId(conn, usuarioId) != null;             
        
        JSONObject dashboardInfoJSON = new JSONObject();
        dashboardInfoJSON.put("isHospedeiro", isHospedeiro);
        dashboardInfoJSON.put("isUsuarioLogado", isUsuarioLogado);
        
        this.buildPerfilJSON(request, usuarioId);
        this.buildAmigosJSON(request, usuarioId);
        this.buildAvaliacoesJSON(request, usuarioId);
        
        session.setAttribute("dashboardInfoJSON", dashboardInfoJSON);        
        response.sendRedirect("dashboard.jsp");
    }
    
    private void buildPerfilJSON(HttpServletRequest request, long usuarioId) {
        Hospedeiro h = HospedeiroDAO.getInstancia().getHospedeiroPorUsuarioId(conn, usuarioId);
        Usuario u = null;

        JSONObject perfilJSON = new JSONObject();
        String nome = "";
        String username = "";
        String endereco = "";
        int lotacao = 0;
        String estaRecebendoViajantes = "";
        JSONArray esportesFavoritos = new JSONArray();
        
        if(h != null) {            
            nome = h.getNome();
            username = h.getUsername();
            endereco = h.getEndereco();
            lotacao = h.getLotacao();
            estaRecebendoViajantes = this.parseEstaRecebendoViajantes(h.getEstaRecebendoViajantes());    
            esportesFavoritos = this.buildEsportesFavoritosJSONArray(h.getUsuarioId());
        } else {
            u = UsuarioDAO.getInstancia().getUsuarioPorId(conn, usuarioId);
           
            nome = u.getNome();
            username = u.getUsername();
            endereco = u.getEndereco();
            lotacao = 0;
            estaRecebendoViajantes = "não";
            esportesFavoritos = this.buildEsportesFavoritosJSONArray(u.getId());
        }       
                
        perfilJSON.put("nome", nome);
        perfilJSON.put("username", username);
        perfilJSON.put("endereco", endereco);
        perfilJSON.put("lotacao", lotacao);
        perfilJSON.put("estaRecebendoViajantes", estaRecebendoViajantes);
        perfilJSON.put("esportesFavoritos", esportesFavoritos);
                   
        HttpSession session = request.getSession();
        session.setAttribute("perfil", perfilJSON);
    }
    
    private JSONArray buildEsportesFavoritosJSONArray(long usuarioId) {
        Set<Esporte> esportesFavoritos = this.getEsportesFavoritosDe(usuarioId);
        JSONArray esportesFavoritosJSONArray = new JSONArray();
        
        for(Esporte e : esportesFavoritos) {
            JSONObject esporteJSON = new JSONObject();
            esporteJSON.put("nome", e.getNome());
            
            esportesFavoritosJSONArray.add(esporteJSON);
        }
        
        return esportesFavoritosJSONArray;
    }
    
    private Set<Esporte> getEsportesFavoritosDe(long usuarioId) {
        Set<EsporteFavorito> esportesFavoritos = EsportesFavoritosDAO.getInstancia().getEsportesFavoritosDe(conn, usuarioId);
        Set<Esporte> esportes = EsportesFavoritosDAO.getInstancia().getEsportesDe(conn, esportesFavoritos);
        return esportes;
    }
    
    private String parseEstaRecebendoViajantes(boolean estaRecebendoViajantes) {
        String text = "não";
        if(estaRecebendoViajantes) {
            text = "sim";
        }        
        return text;
    }
    
    private void buildAmigosJSON(HttpServletRequest request, long usuarioId) {
        HttpSession session = request.getSession();
                
        JSONObject amigosJSON = new JSONObject();
        JSONArray amigosJSONArray = this.buildAmigosJSONArray(usuarioId);        
        amigosJSON.put("amigos", amigosJSONArray);

        session.setAttribute("amigosJSON", amigosJSON);
    }
    
    private JSONArray buildAmigosJSONArray(long usuarioId) {
        
        JSONArray amigosJSONArray = new JSONArray();
        Set<Amigos> amigos = this.getAmigosDe(usuarioId);
        for(Amigos a : amigos) {
            long amigoId = a.getUsuarioId1() == usuarioId ? a.getUsuarioId2() : a.getUsuarioId1();
            Usuario amigo = this.getUsuarioPorId(amigoId);
            
            JSONObject usuarioJSON = new JSONObject();
            usuarioJSON.put("id", Long.toString(amigo.getId()));
            usuarioJSON.put("nome", amigo.getNome());
            
            amigosJSONArray.add(usuarioJSON);
        }
        
        return amigosJSONArray;
    }
    
    private Usuario getUsuarioPorId(long id) {
        return UsuarioDAO.getInstancia().getUsuarioPorId(conn, id);
    }
    
    private Set<Amigos> getAmigosDe(long id) {
        return AmigosDAO.getInstancia().getAmigosDe(conn, id);
    }
    
    private void buildAvaliacoesJSON(HttpServletRequest request, long usuarioId) {
        Set<Avaliacao> avaliacoesSobre = this.getAvaliacoesSobre(usuarioId); 
        
        JSONObject avaliacoesInfoJSON = new JSONObject();
        JSONArray avaliacoesSobreJSONArray = this.buildAvaliacoesSobreJSONArray(avaliacoesSobre, usuarioId);
        JSONArray tiposDeAvaliacaoJSONArray = this.buildTiposDeAvaliacaoJSONArray();
        JSONArray todosOsUsuariosExcetoJSONArray = this.buildTodosOsUsuariosExcetoJSONArray(usuarioId);
        
        avaliacoesInfoJSON.put("avaliacoesSobre", avaliacoesSobreJSONArray);
        avaliacoesInfoJSON.put("tiposDeAvaliacao", tiposDeAvaliacaoJSONArray);
        avaliacoesInfoJSON.put("todosOsUsuarios", todosOsUsuariosExcetoJSONArray);
        
        double mediaGeral = this.calculaMedia(avaliacoesSobre, null);
        double mediaGeralHospedeiro = this.calculaMedia(avaliacoesSobre, TipoAvaliacao.HOSPEDEIRO);
        double mediaGeralHospedado = this.calculaMedia(avaliacoesSobre, TipoAvaliacao.HOSPEDADO);
        double mediaGeralCarona = this.calculaMedia(avaliacoesSobre, TipoAvaliacao.CARONA);
        double mediaGeralCaroneiro = this.calculaMedia(avaliacoesSobre, TipoAvaliacao.CARONEIRO);
        double mediaGeralAmigo = this.calculaMedia(avaliacoesSobre, TipoAvaliacao.AMIGO);
        
        avaliacoesInfoJSON.put("mediaGeral", mediaGeral);
        avaliacoesInfoJSON.put("mediaGeralHospedeiro", mediaGeralHospedeiro);
        avaliacoesInfoJSON.put("mediaGeralHospedado", mediaGeralHospedado);
        avaliacoesInfoJSON.put("mediaGeralCarona", mediaGeralCarona);
        avaliacoesInfoJSON.put("mediaGeralCaroneiro", mediaGeralCaroneiro);
        avaliacoesInfoJSON.put("mediaGeralAmigo", mediaGeralAmigo);
        
        HttpSession session = request.getSession();
        session.setAttribute("avaliacoesInfoJSON", avaliacoesInfoJSON);
    }
    
    private double calculaMedia(Set<Avaliacao> avaliacoes, TipoAvaliacao tipo) {
        double media = 0;
        int soma = 0;
        int quantidadeDeValores = 0;
        
        for(Avaliacao a : avaliacoes) {
            if(tipo == null || a.getTipoAvaliacao() == tipo) {
                soma += a.getNota();
                quantidadeDeValores++;
            }
        }
        if(quantidadeDeValores > 0) {
            media = soma / quantidadeDeValores;
        }        
        return media;
    }
    
    private JSONArray buildAvaliacoesSobreJSONArray(Set<Avaliacao> avaliacoesSobre, long avaliadoId) {          
        JSONArray avaliacoesSobreJSONArray = this.buildAvaliacoesJSONArray(avaliacoesSobre);
        
        return avaliacoesSobreJSONArray;
    }
    
    private JSONArray buildAvaliacoesJSONArray(Set<Avaliacao> avaliacoes) {
        JSONArray avaliacoesJSONArray = new JSONArray();
        for(Avaliacao a : avaliacoes) {
            
            Usuario avaliado = this.getUsuarioPorId(a.getAvaliadoId());
            Usuario avaliador = this.getUsuarioPorId(a.getAvaliadorId());
            
            JSONObject avaliacaoJSON = new JSONObject();
            avaliacaoJSON.put("idAvaliado", Long.toString(avaliado.getId()));
            avaliacaoJSON.put("nomeAvaliado", avaliado.getNome());
            avaliacaoJSON.put("idAvaliador", Long.toString(avaliador.getId()));
            avaliacaoJSON.put("nomeAvaliador", avaliador.getNome());
            avaliacaoJSON.put("nota", Integer.toString(a.getNota()));
            avaliacaoJSON.put("tipo", a.getTipoAvaliacao().toString());
            
            avaliacoesJSONArray.add(avaliacaoJSON);
        }
        
        return avaliacoesJSONArray;
    }
    
    private JSONArray buildTiposDeAvaliacaoJSONArray() {
        JSONArray tiposDeAvaliacaoJSONArray = new JSONArray();
        for(TipoAvaliacao t : TipoAvaliacao.getTodosTiposAvaliacao()) {
            
            JSONObject tipoDeAvaliacaoJSON = new JSONObject();
            tipoDeAvaliacaoJSON.put("nomeTipo", t.toString());
            tipoDeAvaliacaoJSON.put("valorTipo", t.getValorTipoAvaliacao());
            
            tiposDeAvaliacaoJSONArray.add(tipoDeAvaliacaoJSON);
        }
        
        return tiposDeAvaliacaoJSONArray;
    }     
    
    private JSONArray buildTodosOsUsuariosExcetoJSONArray(long usuarioId) {
        JSONArray todosOsUsuariosJSONArray = new JSONArray();
        Set<Usuario> todosOsUsuarios = this.getTodosUsuariosExceto(usuarioId);
        for(Usuario u : todosOsUsuarios) {
            
            JSONObject usuarioNomeJSON = new JSONObject();
            usuarioNomeJSON.put("id", Long.toString(u.getId()));
            usuarioNomeJSON.put("nome", u.getNome());
            
            todosOsUsuariosJSONArray.add(usuarioNomeJSON);
        }
        
        return todosOsUsuariosJSONArray;
    }
    
    private Set<Avaliacao> getAvaliacoesSobre(long avaliadoId) {
        Set<Avaliacao> todasAvaliacoesSobre = AvaliacaoDAO.getInstancia().getAvaliacoesSobre(conn, avaliadoId);
        Set<Avaliacao> avaliacoesSobre = new HashSet<>();
        
        for(Avaliacao a : todasAvaliacoesSobre) {
            Avaliacao avaliacaoComplementar = AvaliacaoDAO.getInstancia().getAvaliacaoComplementar(conn, a.getAvaliadoId(), a.getAvaliadorId(), a.getTipoAvaliacao().getValorTipoAvaliacao());
            if(avaliacaoComplementar != null) {
                avaliacoesSobre.add(a);
            }
        }
        
        return avaliacoesSobre;
    }
    
    private Set<Usuario> getTodosUsuariosExceto(long usuarioId) {                
        return UsuarioDAO.getInstancia().getTodosOsUsuariosExceto(conn, usuarioId);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }
}
