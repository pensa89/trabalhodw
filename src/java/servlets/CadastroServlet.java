package servlets;

import database.EsporteDAO;
import database.EsportesFavoritosDAO;
import database.HospedeiroDAO;
import database.UsuarioDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Esporte;
import models.Hospedeiro;
import models.Usuario;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;

@WebServlet(name = "CadastroServlet", urlPatterns = {"/cadastro"})
public class CadastroServlet extends HttpServlet {
    
    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        List<Esporte> esportes = EsporteDAO.getInstancia().getEsportes(conn);
        JSONArray esportesJSONArray = this.buildEsportesJSONArray(esportes);        
        
        HttpSession session = request.getSession();
        session.setAttribute("esportesJSONArray", esportesJSONArray);        
        
        response.sendRedirect("cadastro.jsp"); 
    }
    
    private JSONArray buildEsportesJSONArray(List<Esporte> esportes) {
        JSONArray esportesJSONArray = new JSONArray();
        
        for (Esporte e : esportes) {
            JSONObject esporteJSON = new JSONObject();
            
            esporteJSON.put("valorEsporte", e.getId());
            esporteJSON.put("nomeEsporte", e.getNome());
            
            esportesJSONArray.add(esporteJSON);
        }        
        return esportesJSONArray;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String username = request.getParameter("username");
        
        Usuario usuarioAntigo = UsuarioDAO.getInstancia().getUsuarioPorUsername(conn, username);
        if(usuarioAntigo != null) {
            JSONObject errosJSON = new JSONObject();
            errosJSON.put("mensagem", "Nome de usuário em uso, escolha outro.");

            HttpSession session = request.getSession();
            session.setAttribute("erros", errosJSON);
            
            response.sendRedirect("cadastro");
        } else {
            String nome = request.getParameter("nome");
            String senha = request.getParameter("senha");
            String endereco = request.getParameter("endereco");
            long esporteFavoritoId = Long.parseLong(request.getParameter("esporteFavorito"));
            String isHospedeiro = request.getParameter("hospedeiro");
            String estaRecebendoViajantesParameter = request.getParameter("estaRecebendoViajantes");
                                 
            Usuario u = new Usuario(nome, endereco, username, senha);            
            Usuario novoUsuario = UsuarioDAO.getInstancia().insereNovoUsuario(conn, u);
            long usuarioId = novoUsuario.getId();
            EsportesFavoritosDAO.getInstancia().insereNovoEsporteFavorito(conn, usuarioId, esporteFavoritoId);
            
            if(isHospedeiro != null) {
                int lotacao = Integer.parseInt(request.getParameter("lotacao"));
                String pais = request.getParameter("pais");
                String cidade = request.getParameter("cidade");
            
                boolean estaRecebendoViajantes = estaRecebendoViajantesParameter != null;
                Hospedeiro h = new Hospedeiro(usuarioId, nome, endereco, username, senha, estaRecebendoViajantes, lotacao, pais, cidade);
                HospedeiroDAO.getInstancia().insereNovoHospedeiro(conn, h);
            }      

            HttpSession session = request.getSession();
            session.setAttribute("usuarioLogadoId", usuarioId);

            response.sendRedirect("dashboard.jsp");
        }
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    } 
}
