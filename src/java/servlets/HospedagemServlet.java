package servlets;

import database.HospedeiroDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Hospedeiro;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;

@WebServlet(name = "HospedagemServlet", urlPatterns = {"/hospedagem"})
public class HospedagemServlet extends HttpServlet {
    
    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        long usuarioId = (long) session.getAttribute("usuarioLogadoId");
          
        response.sendRedirect("hospedagem.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String pais = request.getParameter("pais");
        String cidade = request.getParameter("cidade");
        
        Set<Hospedeiro> resultadosDaBusca = HospedeiroDAO.getInstancia().getHospedeirosPorQuery(conn, pais, cidade);
        JSONArray resultadosDaBuscaJSONArray = this.buildResultadosDaBusca(resultadosDaBusca);
        
        HttpSession session = request.getSession();
        session.setAttribute("resultadosJSON", resultadosDaBuscaJSONArray);
          
        response.sendRedirect("hospedagem.jsp");
    }
    
    private JSONArray buildResultadosDaBusca(Set<Hospedeiro> hospedeiros) {
        JSONArray resultados = new JSONArray();
        
        for(Hospedeiro h : hospedeiros) {
            JSONObject resultado = new JSONObject();
            resultado.put("id", h.getHospedeiroId());
            resultado.put("nome", h.getNome());
            resultado.put("pais", h.getPais());
            resultado.put("cidade", h.getCidade());
            resultado.put("lotacao", h.getLotacao());            
            
            resultados.add(resultado);
        }
        
        return resultados;
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }
}
