package servlets;

import database.ReservasDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseCredentials;

@WebServlet(name = "AceitarReservaServlet", urlPatterns = {"/aceitar-reserva"})
public class AceitarReservaServlet extends HttpServlet {

    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        long reservaId = Long.parseLong(request.getParameter("reservaId"));
        ReservasDAO.getInstancia().atualizaStatusReserva(conn, reservaId, "aprovada");        
        response.sendRedirect("hospedeiro");
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }
}
