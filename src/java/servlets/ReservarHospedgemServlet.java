package servlets;

import database.HospedeiroDAO;
import database.ReservasDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.Hospedeiro;
import models.Reserva;
import org.json.simple.JSONObject;
import util.DatabaseCredentials;

@WebServlet(name = "ReservarHospedgemServlet", urlPatterns = {"/reservar-hospedagem"})
public class ReservarHospedgemServlet extends HttpServlet {
    
    private Connection conn = null;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        session.setAttribute("hospedeiroId", request.getParameter("hospedeiroId"));
        
        response.sendRedirect("reservarHospedagem.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        
        long usuarioId = (long) session.getAttribute("usuarioLogadoId");
        long hospedeiroId = Long.parseLong(request.getParameter("hospedeiroId"));
        int totalDeViajantes = Integer.parseInt(request.getParameter("totalDeViajantes"));
        int totalDePraticantes = Integer.parseInt(request.getParameter("totalDePraticantes"));
        
        String dataInicioString = request.getParameter("dataInicio");
        String dataFimString = request.getParameter("dataFim");

        Date dataInicio = null;
        Date dataFim = null;

        try {
            dataInicio = new SimpleDateFormat("yyyy-MM-dd").parse(dataInicioString);
            dataFim = new SimpleDateFormat("yyyy-MM-dd").parse(dataFimString);
        } catch (ParseException ex) {
            JSONObject errosJSON = new JSONObject();
            errosJSON.put("mensagem", "Formato de data inválido, tente novamente.");            
            session.setAttribute("erros", errosJSON);
            
            response.sendRedirect("reservar-hospedagem");
        }
        
        String mensagemDeErro = "";
        Hospedeiro h = HospedeiroDAO.getInstancia().getHospedeiroPorId(conn, hospedeiroId);
        
        if(h.getLotacao() < totalDePraticantes + totalDeViajantes) {
            mensagemDeErro += "Total de viajantes e praticantes excede a lotação do hospedeiro. ";
        }
        
        if(!h.getEstaRecebendoViajantes()) {
            mensagemDeErro += "Hospedeiro inválido. ";
        }
        
        if(dataInicio.after(dataFim) || dataFim.before(dataInicio)) {
            mensagemDeErro += "Data de início e fim de reserva inválidas, tente novamente. ";
        }
        
        Reserva reservaNoPeriodo = ReservasDAO.getInstancia().getReservaNoPeriodo(conn, hospedeiroId, dataInicio, dataFim);
        if(reservaNoPeriodo != null) {
            mensagemDeErro += "Já existe uma reserva nesse período, tente novamente. ";
        }
        
        if(!mensagemDeErro.isEmpty()) {
            JSONObject errosJSON = new JSONObject();
            errosJSON.put("mensagem", mensagemDeErro);            
            session.setAttribute("erros", errosJSON);
            
            response.sendRedirect("reservar-hospedagem");
        } else {
            Reserva novaReserva = new Reserva(usuarioId, hospedeiroId, dataInicio, dataFim, totalDeViajantes, totalDePraticantes);
            ReservasDAO.getInstancia().insereNovaReserva(conn, novaReserva);
            
            session.removeAttribute("erros");
            response.sendRedirect("hospedagem");
        }
    }
    
    @Override
    public void init() throws ServletException {        
        try { 
            conn = DriverManager.getConnection(DatabaseCredentials.getDatabaseURL(), DatabaseCredentials.getDatabaseUser(), DatabaseCredentials.getDatabasePassword());
        } catch (Exception ex) {
        }
    }
    
    @Override
    public void destroy() {
        try {
            conn.close();
        } catch (Exception ex) {
        }
    }
}
