<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de estadia</title>
    </head>
    <body>
        <h2>Cadastro</h2>             
        
        <% 
            JSONArray esportesJSONArray = (JSONArray) session.getAttribute("esportesJSONArray");
        %>
        
        <div>
            <%if(session.getAttribute("erros") != null){ %>
                <% 
                    JSONObject errosJSON = (JSONObject) session.getAttribute("erros");                
                %>
                <div style="color: red;">
                    <%= errosJSON.get("mensagem") %><br />
                </div>
                <br />                
            <%}%>
            <form action="cadastro" method="post">
                <label for="username">Nome de usuário: </label>            
                <input type="text" id="username" name="username" placeholder="Login" required><br /><br />
                
                <label for="nome">Nome: </label>            
                <input type="text" id="nome" name="nome" placeholder="Nome" required><br /><br />

                <label for="senha">Senha: </label>            
                <input type="password" id="senha" name="senha" placeholder="Senha" required><br /><br />
                
                <label for="endereco">Endereço: </label>            
                <input type="text" id="endereco" name="endereco" placeholder="Endereço" required><br /><br />
                
                <label for="esporteFavorito">Esporte favorito: </label>
                <select id="esporteFavorito" name="esporteFavorito">
                    <%for (int i = 0; i < esportesJSONArray.size(); i++){ %>
                        <% JSONObject esporteFavorito = (JSONObject) esportesJSONArray.get(i); %>

                        <option value="<%= esporteFavorito.get("valorEsporte") %>"><%= esporteFavorito.get("nomeEsporte").toString() %></option>
                    <%}%>
                </select><br /><br />
                
                <input type="checkbox" id="hospedeiro" name="hospedeiro">É hospedeiro?<br /><br />
                
                <label for="lotacao">Lotação máxima: </label>            
                <input type="number" id="lotacao" name="lotacao" value="1" min="1"><br /><br />
                
                <label for="endereco">País: </label>            
                <input type="text" id="pais" name="pais" placeholder="País"><br /><br />
                
                <label for="endereco">Cidade: </label>            
                <input type="text" id="cidade" name="cidade" placeholder="Cidade"><br /><br />
                
                <input type="checkbox" id="estaRecebendoViajantes" name="estaRecebendoViajantes">Está recebendo viajantes?<br /><br />

                <button type="submit">Cadastrar</button>
            </form>
        </div>
    </body>
</html>
