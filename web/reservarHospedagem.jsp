<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de estadia</title>
    </head>
    <body>
        <h2>Reserva de hospedagem</h2>
        
        <a href='dashboard'>Home</a><br />
        <a href='logout'>Encerrar sessão</a><br /><br />
        
        <div>
            
            <%if(session.getAttribute("erros") != null){ %>
                <% 
                    JSONObject errosJSON = (JSONObject) session.getAttribute("erros");                
                %>
                <div style="color: red;">
                    <%= errosJSON.get("mensagem") %><br />
                </div>
                <br />                
            <%}%>
            
            <form action="reservar-hospedagem" method="post">         
                <input type="hidden" id="hospedeiroId" name="hospedeiroId" value='<%= session.getAttribute("hospedeiroId") %>'>
                
                <label for="totalDeViajantes">Total de viajantes: </label>            
                <input type="number" id="totalDeViajantes" name="totalDeViajantes" value="0" min="0"><br /><br />
                
                <label for="totalDePraticantes">Total de praticantes: </label>
                <input type="number" id="totalDePraticantes" name="totalDePraticantes" value="0" min="0"><br /><br />
                
                <label for="dataInicio">Data de início da reserva: </label>            
                <input type="date" id="dataInicio" name="dataInicio" required><br /><br />
                
                <label for="dataFim">Data de fim da reserva</label>            
                <input type="date" id="dataFim" name="dataFim" required><br /><br />
                
                <button type="submit">Finalizar reserva</button>
            </form>
        </div>
        <br /><br />
    </body>
</html>
