<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de estadia</title>
    </head>
    <body>
        <h2>Reservas solicitadas</h2>
        
        <a href='dashboard'>Home</a><br />
        <a href='logout'>Encerrar sessão</a><br /><br />

        <div>
             <% 
                JSONObject reservasInfoJSON = (JSONObject) session.getAttribute("reservasInfoJSON");
                JSONArray reservas = (JSONArray) reservasInfoJSON.get("reservas");
            %>
            
            <%for (int i = 0; i < reservas.size(); i++){ %>
                <% JSONObject reserva = (JSONObject) reservas.get(i); %>

                Nome do requisitante: <a href='dashboard?id=<%= reserva.get("idUsuario") %>'> <%= reserva.get("nome") %></a><br />
                Período: <%= reserva.get("dataInicio") %> até <%= reserva.get("dataFim") %><br />
                Total de praticantes: <%= reserva.get("totalDePraticantes") %><br />
                Total de viajantes: <%= reserva.get("totalDeViajantes") %><br />
                Status: <%= reserva.get("status") %>
                <%if(reserva.get("status").equals("aprovada")) {%>
                    <form action="reservas" method="POST">
                        <input type="hidden" id="reservaId" name="reservaId" value='<%= reserva.get("idReserva") %>'>
                        <button type="submit">Confirmar</button>
                    </form>
                <%}%>
            <%}%>
        </div>
    </body>
</html>
