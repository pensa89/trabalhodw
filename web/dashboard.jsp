<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de estadia</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            
            th, td {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 5px;
                padding-bottom: 5px;
            }
        </style>
    </head>
    <body>
        <% 
            JSONObject dashboardInfoJSON = (JSONObject) session.getAttribute("dashboardInfoJSON");
        %>
        
        <h2>Dashboard</h2> 
        <a href='dashboard'>Home</a><br /><br />
        
        <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
            <%if((Boolean) dashboardInfoJSON.get("isHospedeiro")) {%>
                <a href="hospedeiro">Ver solicitações de hospedagem</a>
            <%} else {%>
                <a href="hospedagem">Buscar hospedagem</a><br />
                <a href="reservas">Ver reservas solicitadas</a>
            <%}%><br /> 
        <%}%>

        <a href='logout'>Encerrar sessão</a><br /><br />
        <div>           
            <div>
                <% 
                    JSONObject perfilJSON = (JSONObject) session.getAttribute("perfil");
                    JSONArray esportesFavoritos = (JSONArray) perfilJSON.get("esportesFavoritos");
                %>
                <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
                    <h3>Meu perfil:</h3>
                    Nome: <%= perfilJSON.get("nome") %><br />
                <%} else {%>
                    <h3>Perfil de <%= perfilJSON.get("nome") %>: </h3>
                <%}%>                
                Nome de usuário: <%= perfilJSON.get("username") %><br />
                Endereço: <%= perfilJSON.get("endereco") %><br />
                Está recebendo viajantes: <%= perfilJSON.get("estaRecebendoViajantes") %><br />
                Lotação: <%= perfilJSON.get("lotacao") %><br />          
                Esportes favoritos: 
                <ul>
                    <%for (int i = 0; i < esportesFavoritos.size(); i++){ %>
                        <% JSONObject esporteFavorito = (JSONObject) esportesFavoritos.get(i); %>

                        <li>
                            <%= esporteFavorito.get("nome") %>
                        </li>
                    <%}%>
                </ul>
            </div>            
            
            <div>
                <% 
                    JSONObject amigosJSON = (JSONObject) session.getAttribute("amigosJSON");
                    JSONArray amigos = (JSONArray) amigosJSON.get("amigos");
                %>
                <h3>Amigos:</h3>
                <%for (int i = 0; i < amigos.size(); i++){ %>
                    <% JSONObject u = (JSONObject) amigos.get(i); %>

                    <a href='dashboard?id=<%= u.get("id") %>'><%= u.get("nome") %></a><br />
                <%}%>
            </div><br /><br />
            
            <!--<a href="avaliacao?id=">Ver todas as avaliações</a><br />-->
            
            <% 
            JSONObject avaliacoesInfoJSON = (JSONObject) session.getAttribute("avaliacoesInfoJSON");
            JSONArray avaliacoesSobre = (JSONArray) avaliacoesInfoJSON.get("avaliacoesSobre");
            JSONArray tiposDeAvaliacao = (JSONArray) avaliacoesInfoJSON.get("tiposDeAvaliacao");
            JSONArray todosOsUsuarios = (JSONArray) avaliacoesInfoJSON.get("todosOsUsuarios");
            
            Double mediaGeral = (Double) avaliacoesInfoJSON.get("mediaGeral");
            Double mediaGeralHospedeiro = (Double) avaliacoesInfoJSON.get("mediaGeralHospedeiro");
            Double mediaGeralHospedado = (Double) avaliacoesInfoJSON.get("mediaGeralHospedado");
            Double mediaGeralCaroneiro = (Double) avaliacoesInfoJSON.get("mediaGeralCaroneiro");
            Double mediaGeralCarona = (Double) avaliacoesInfoJSON.get("mediaGeralCarona");
            Double mediaGeralAmigo = (Double) avaliacoesInfoJSON.get("mediaGeralAmigo");
        %>

        <div>
            <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
                <h3>Fazer nova avaliação</h3>

                <%if(session.getAttribute("erros") != null){ %>
                    <% 
                        JSONObject errosJSON = (JSONObject) session.getAttribute("erros");                
                    %>
                    <div style="color: red;">
                        <%= errosJSON.get("mensagem") %><br />
                    </div>
                    <br />                
                <%}%>

                <form action="avaliacao" method="post">
                    <select id="avaliadoId" name="avaliadoId">
                        <%for (int i = 0; i < todosOsUsuarios.size(); i++){ %>
                            <% JSONObject usuario = (JSONObject) todosOsUsuarios.get(i); %>

                            <option value="<%= usuario.get("id") %>"><%= usuario.get("nome").toString() %></option>
                        <%}%>
                    </select>

                    <select id="tipoAvaliacao" name="tipoAvaliacao">
                        <%for (int i = 0; i < tiposDeAvaliacao.size(); i++){ %>
                            <% JSONObject tipoDeAvaliacao = (JSONObject) tiposDeAvaliacao.get(i); %>

                            <option value="<%= tipoDeAvaliacao.get("valorTipo") %>"><%= tipoDeAvaliacao.get("nomeTipo").toString() %></option>
                        <%}%>
                    </select>

                    <input type="number" name="nota" id="nota" value="0" min="0" max="5" required>

                    <button type="submit">Avaliar</button>
                </form>
            <%}%>
        </div>
        <br />   
        <div>
            <b>Média geral de avaliações:</b> <%= mediaGeral %><br />
            <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
                <b>Média de avaliações sobre mim como hospedeiro:</b> <%= mediaGeralHospedeiro %><br />
                <b>Média de avaliações sobre mim como hospedado:</b> <%= mediaGeralHospedado %><br />
                <b>Média de avaliações sobre mim como caroneiro:</b> <%= mediaGeralCaroneiro %><br />
                <b>Média de avaliações sobre mim como carona:</b> <%= mediaGeralCarona %><br />
                <b>Média de avaliações sobre mim como amigo:</b> <%= mediaGeralAmigo %><br />            
            <%}%>                       
        </div>
        
        <div>
            <h3>Todas as avaliações</h3>
            
                <table>
                    <tr>
                        <th>Avaliador</th>
                        <th>Tipo de avaliação</th> 
                        <th>Nota</th>
                    </tr>            

                    <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                        <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>

                        <tr>
                            <td>
                                <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                            </td>
                            <td><%= avaliacaoSobre.get("tipo") %></td> 
                            <td><%= avaliacaoSobre.get("nota") %></td>
                        </tr>
                    <%}%> 
                </table>           
        </div>
        
        <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
            <div>

                <h3>Avaliações de amigos sobre mim</h3>

                    <table>
                        <tr>
                            <th>Avaliador</th>
                            <th>Tipo de avaliação</th> 
                            <th>Nota</th>
                        </tr>            

                        <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                            <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>

                            <%if(avaliacaoSobre.get("tipo") == "Amigo") {%>
                                <tr>
                                    <td>
                                        <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                                    </td>
                                    <td><%= avaliacaoSobre.get("tipo") %></td> 
                                    <td><%= avaliacaoSobre.get("nota") %></td>
                                </tr>
                            <%}%>                        
                        <%}%> 
                    </table>           
            </div>           
        
            <div>
                <h3>Avaliações de hospedeiro sobre mim</h3>

                    <table>
                        <tr>
                            <th>Avaliador</th>
                            <th>Tipo de avaliação</th> 
                            <th>Nota</th>
                        </tr>            

                        <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                            <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>
                            <% String tipoAvaliacao = (String) avaliacaoSobre.get("tipo"); %>
                            <%if(tipoAvaliacao.equalsIgnoreCase("Hospedeiro")) {%>
                                <tr>
                                    <td>
                                        <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                                    </td>
                                    <td><%= avaliacaoSobre.get("tipo") %></td> 
                                    <td><%= avaliacaoSobre.get("nota") %></td>
                                </tr>
                            <%}%>                        
                        <%}%> 
                    </table>           
            </div>

            <div>
                <h3>Avaliações de hospedado sobre mim</h3>

                    <table>
                        <tr>
                            <th>Avaliador</th>
                            <th>Tipo de avaliação</th> 
                            <th>Nota</th>
                        </tr>            

                        <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                            <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>

                            <%if(avaliacaoSobre.get("tipo") == "Hospedado") {%>
                                <tr>
                                    <td>
                                        <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                                    </td>
                                    <td><%= avaliacaoSobre.get("tipo") %></td> 
                                    <td><%= avaliacaoSobre.get("nota") %></td>
                                </tr>
                            <%}%>                        
                        <%}%> 
                    </table>           
            </div>

            <div>
                <h3>Avaliações de caroneiro sobre mim</h3>

                    <table>
                        <tr>
                            <th>Avaliador</th>
                            <th>Tipo de avaliação</th> 
                            <th>Nota</th>
                        </tr>            

                        <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                            <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>

                            <%if(avaliacaoSobre.get("tipo") == "Caroneiro") {%>
                                <tr>
                                    <td>
                                        <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                                    </td>
                                    <td><%= avaliacaoSobre.get("tipo") %></td> 
                                    <td><%= avaliacaoSobre.get("nota") %></td>
                                </tr>
                            <%}%>                        
                        <%}%> 
                    </table>           
            </div>

            <div>
                <h3>Avaliações de carona sobre mim</h3>

                    <table>
                        <tr>
                            <th>Avaliador</th>
                            <th>Tipo de avaliação</th> 
                            <th>Nota</th>
                        </tr>            

                        <%for (int i = 0; i < avaliacoesSobre.size(); i++){ %>
                            <% JSONObject avaliacaoSobre = (JSONObject) avaliacoesSobre.get(i); %>

                            <%if(avaliacaoSobre.get("tipo") == "Carona") {%>
                                <tr>
                                    <td>
                                        <a href='dashboard?id=<%= avaliacaoSobre.get("idAvaliador") %>'><%= avaliacaoSobre.get("nomeAvaliador") %></a><br />
                                    </td>
                                    <td><%= avaliacaoSobre.get("tipo") %></td> 
                                    <td><%= avaliacaoSobre.get("nota") %></td>
                                </tr>
                            <%}%>                        
                        <%}%> 
                    </table>           
            </div>
        <%}%>
        
        <br />
            
        <%if((Boolean) dashboardInfoJSON.get("isUsuarioLogado")) {%>
            <%if((Boolean) dashboardInfoJSON.get("isHospedeiro")) {%>
                <a href="hospedeiro">Ver solicitações de hospedagem</a>
            <%} else {%>
                <a href="hospedagem">Buscar hospedagem</a>
            <%}%><br /> 
        <%}%>

        <a href='logout'>Encerrar sessão</a><br /><br />
        </div>        
    </body>
</html>
