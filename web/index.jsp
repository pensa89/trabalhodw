<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sistema de estadia</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h2>Sistema de estadia - tela de autenticação</h2>
        <div>             
            <%if(session.getAttribute("erros") != null){ %>
                <% 
                    JSONObject errosJSON = (JSONObject) session.getAttribute("erros");                
                %>
                <div style="color: red;">
                    <%= errosJSON.get("mensagem") %><br />
                </div>
                <br />                
            <%}%>            
                       
            <form action="login" method="post">
                <label for="login">Login: </label>            
                <input type="text" id="username" name="username" placeholder="Login" required>

                <label for="senha">Senha: </label>            
                <input type="password" id="senha" name="senha" placeholder="Senha" required>

                <button type="submit">Entrar</button>
            </form>
            <br />
            <a href="cadastro">Criar nova conta</a>
        </div>
    </body>
</html>

