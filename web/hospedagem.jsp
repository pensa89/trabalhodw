<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de estadia</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
            
            th, td {
                padding-left: 15px;
                padding-right: 15px;
                padding-top: 5px;
                padding-bottom: 5px;
            }
        </style>
    </head>
    <body>
        <h2>Hospedagem</h2>
        
        <a href='dashboard'>Home</a><br />
        <a href='logout'>Encerrar sessão</a><br /><br />
        
       <% 
            JSONArray resultadosJSON = null;
            if(session.getAttribute("resultadosJSON") != null) {
                resultadosJSON = (JSONArray) session.getAttribute("resultadosJSON");
            }           
        %>
        
        <div>
            <form action="hospedagem" method="post">
                <label for="endereco">País: </label>            
                <input type="text" id="pais" name="pais" placeholder="País" required><br /><br />
                
                <label for="endereco">Cidade: </label>            
                <input type="text" id="cidade" name="cidade" placeholder="Cidade" required><br /><br />
                
                <button type="submit">Procurar</button>
            </form>
        </div>
        <br /><br />
        <div>
            <%if(resultadosJSON != null) {%>
                <table>
                    <tr>
                        <th>Hospedeiro</th>
                        <th>País</th> 
                        <th>Cidade</th>
                        <th>Lotação</th>
                        <th></th>
                    </tr>
                    
                    <%for(int i = 0; i < resultadosJSON.size(); i++) {%>
                        <% JSONObject resultado = (JSONObject) resultadosJSON.get(i); %>

                        <td><%= resultado.get("nome") %></td> 
                        <td><%= resultado.get("pais") %></td> 
                        <td><%= resultado.get("cidade") %></td> 
                        <td><%= resultado.get("lotacao") %></td> 
                        <td><a href="reservar-hospedagem?hospedeiroId=<%= resultado.get("id") %>">Reservar</a></td> 
                    <%}%>
                </table>
            <%}%>
        </div>
    </body>
</html>
